# Duplicate issue detector

This is a Python notebook that uses the [SBERT](https://sbert.net/docs/quickstart.html) models to detect the semantic
meaning of Issue titles and provide a confidence level between 0 and 1. The closer it is to 1 the higher the probablity
of a duplicate issue.

## How to run?

### Requirements

* Python 3.10.X

#### Using Visual Studio Code

* Install the recommended extensions
* Select the Python Kernel from top-right corner

![Python Kernel](./doc/images/python-kernel.png)

* Click on "Run All"

### FAQ

1. This notebook can be run locally as it uses the SBERT models for NLP from HuggingFace
2. Using titles seems to be the best balance between performance and accuracy, using the entire description might require a different approach than SBERT
3. Confidences below 0.5 are discarded
